#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>

void boHTML(FILE* out);
void eoHTML(FILE* out);

void rewrite(FILE* in, FILE* out);

int main(int argc, char* argv[]){
	FILE* fpOUT;
	FILE* fpIn;
	fpOUT = fopen(argv[1], "w");
	if (fpOUT == NULL){
		fprintf(stderr, "fpOUT = NULL\n");
		return EXIT_FAILURE;
	}
	boHTML(fpOUT);
	fpIn = fopen(argv[2], "r");
	if (fpIn == NULL){
		fprintf(stderr, "fpIn = NULL on f1\n");
		return EXIT_FAILURE;
	}
	rewrite(fpIn, fpOUT);
	fclose(fpIn);
	if(argc == 4){
		fpIn = fopen(argv[3], "r");
		if (fpIn == NULL){
			fprintf(stderr, "fpIn = NULL on f2\n");
			return EXIT_FAILURE;
		}
		rewrite(fpIn, fpOUT);
		fclose(fpIn);
	}
	eoHTML(fpOUT);
	fclose(fpOUT);
	return EXIT_SUCCESS;
}

void rewrite(FILE* in, FILE* out){
	int inTXT = 0;
	char buf[5000] = {'\0'};
	fgets(buf, 5000, in);
	while(fgets(buf, 5000, in)){
		char* MainTxt;
		for (int i = 0; i < 5000; i++)
		{
			if((buf[i] == '<') || (buf[i] == '>')){
				buf[i] = ' ';
			}
		}
		if( (isdigit(buf[0]) ) && (buf[2] == '.') ){
			if(inTXT){
				fprintf(out, "</td>\n</tr>\n<tr>\n<td class=\"dtcell\">\n");
			} else {
				fprintf(out, "<tr>\n<td class=\"dtcell\">\n");
				inTXT = 1;
			}
			char* date = strndup(buf, 15);
			fprintf(out, "%s", date);
			free(date);
			MainTxt = buf+17;
			for(int i = 0; *MainTxt != ':'; i++){
				fprintf(out, "%c", *MainTxt);
				MainTxt++;
			}
			MainTxt+=2;
			fprintf(out, "</td>\n<td class=\"txtcell\">\n");
			fprintf(out, "%s<br>\n", MainTxt);
		} else {
			//cot
			fprintf(out, "%s<br>\n", buf);
		}
	}
	fprintf(out, "</td></tr>\n");
}

void boHTML(FILE* out){
	fprintf(out,
"<!DOCTYPE html>\n"
"<html>\n"
"<head>\n"
"<meta charset=\"UTF-8\">\n"
"<style>\n"
"table {width: 100%; table-layout: fixed;}\n"
"table, td{border: 2px solid black; border-collapse: collapse;}\n"
"         tr {\n"
"                background-color: #fffbf0;\n"
"                color: #000;\n"
"               }\n"
"         tr:nth-child(odd) {\n"
"                background-color: #e4ebf2 ;\n"
"               }\n"
"			td.dtcell{\n"
"				width: 15%;\n"
"			}\n"
"			td.txtcell{\n"
"				width: 85%;\n"
"			}\n"
"}\n"
"</style>\n"
"</head>\n"
"<body>\n"
"<table>\n");
}

void eoHTML(FILE* out){
	fprintf(out,
"</table>\n"
"</body>\n"
"</html>");
}
