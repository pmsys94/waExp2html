CFILE=waexp2html.c
BDIR=build
APP=waExp2html

all: ${APP}


${APP}: build-dir 
	${CC} -o ${BDIR}/${APP} ${CFILE}

build-dir:
	mkdir -p ${BDIR}