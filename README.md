# WhatsApp TXT exports to HTML
## A quick-and-dirty programm to convert WhatsApp chat exports done in ASCII txt files to HTML files

**This programm was developed for german chat exports and also only tested with them. Therefore the following description of the usage is written in german.**

Mit diesem Tool kann man seine WhatsApp Chat exporte in HTML Seiten umwandeln. Wenn man (zb per E-Mail) einen Chat exportiert, wird dieser in einer TXT Datei ausgegeben. Dies ist eine sehr schlecht lesbare form. Das Programm selbst kann nicht viel. Es ist eigentlich nur entstanden, weil es besser war, die Chats in HTML als Tabellen lesen zu können. Aber es nimmt einem damit die Handarbeit ab, jeder Nachricht von Hand ein HTML Tag geben zu müssen.

### Kompilieren

Das Programm wurde unter Linux mit dem Standad C Compiler kompiliert. 

Es wurde ein für den verwendeten Befehl ein Makefile in das Projekt gepackt.

Hinweis: Teile des Markups sind hardcoded als Strings im C Code. Um die Ausgabe HTML anzupassen (z.B. Header um Tabelle mit CSS zu stylen), müssen die Strings angepasst werden und danach das Programm mit dem Makefile erneut kompiliert werden.
Ferner wird in die Ausgabe HTML Datei inline CSS geschrieben, um die Tabelle zu stylen. Das bietet die möglichkeit, eine Datei auszuliefern. Außerdem soll die Datei einfach vom Dateisystem aus aufgerufen werden können und keinen Webwerver dafür erfordern.

### Basic usage
Erlaubt die Verwendung mit einer Output Datei (OF), einer eingabe Datei von WhatsApp (IF1) und optional einer zweiten Datei (IF2).
Die Datei muss mit einer sinnlosen (nicht Nachrricht) Zeile beginnen. Zum Beispiel: "WhatsApp Chats sind nun Ende-zu-Ende Verschlüsselt".

Vom Build Verzeichnis aus:
`./waExp2html <OF> <IF1> [<IF2>]`

Zum Beispiel:

`./waExp2html WA_with_my_friend.html wa_my_friend.txt`

### Verwendung nach dem Kompilieren:

1. In WhatsApp einen Chat auswählen  - (1:1) oder Gruppe
2. Menü -> 'Chat exportieren'
3. (Ich testete mit E-Mail, ich weiß nicht, wie es mit anderen optionen ist) - E-Mail wählen -> Ziel Adresse der Maill ist natürlich privater Account
    + wenn mit E-Mail, dann ist natürlich öffnen der Mail nötig und speichern der txt Datei -> Hinweis: einen kurzen Namen für die ASCII Datei verwenden, statt der von WhatsApp verwendeten Namen. Das ist auf der Komandozeile besser.
4. ASCII Datei öffnen mit einem Editor, mit dem man suchen und ersetzen kann.
5. Nach bestimmten schlüsselwörtern suchen, die sonst dazu führen, dass die Tabelle in der HTML Datei nicht funktioniert - In diesen nach dem Namen / der Telefonnummer einen : einfügen oder die Zeile Entfernen. Schlüsselwörter sind:
    + verlassen
    + hinzugefügt
    + entfernt
    + Gruppenbild
    + Betreff
    + verschlüsselt -> Kann als erste dummy Zeile verwendet werden
6. Speichern der Änderungen
7. Ausführen des Programms auf der Komandozeile erzeugt nun eine HTML Datei. (siehe Beispiel)
